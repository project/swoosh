import {onCLS, onFID, onLCP, onTTFB, onFCP, onINP} from 'https://unpkg.com/web-vitals@3?module';

const queue = new Set();
function addToQueue(metric) {
  queue.add(metric);
}

function flushQueue() {
  if (queue.size > 0 && drupalSettings.swoosh !== undefined) {
    const vitals = [...queue];
    const payload = {
      site: drupalSettings.swoosh.site,
      vitals,
      page: document.location.href,
      connection: window.navigator.connection?.effectiveType,
      userAgent: window.navigator.userAgent,
      template: drupalSettings.swoosh.nodeType,
      screenHeight: window.screen.availHeight,
      screenWidth: window.screen.availWidth,
      authenticated: drupalSettings.user?.uid > 0,
      navigationType: performance.getEntriesByType("navigation")[0].type

    }
    const body = JSON.stringify(payload);

    fetch(drupalSettings.swoosh.endpoint, {body, method: 'POST', keepalive: true, headers: { 'Content-Type': 'application/json' }});

    queue.clear();
  }
}

onCLS(addToQueue);
onFID(addToQueue);
onLCP(addToQueue);
onTTFB(addToQueue);
onFCP(addToQueue);
onINP(addToQueue);

// Report all available metrics whenever the page is backgrounded or unloaded.
addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'hidden') {
    flushQueue();
  }
});

// NOTE: Safari does not reliably fire the `visibilitychange` event when the
// page is being unloaded. If Safari support is needed, you should also flush
// the queue in the `pagehide` event.
addEventListener('pagehide', flushQueue);
