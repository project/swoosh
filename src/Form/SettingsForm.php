<?php

declare(strict_types=1);

namespace Drupal\swoosh\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Web Vitals Reporter settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'swoosh_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['swoosh.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->config('swoosh.settings')->get('status'),
    ];
    $form['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#required' => TRUE,
      '#default_value' => $this->config('swoosh.settings')->get('site'),
    ];
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#required' => TRUE,
      '#default_value' => $this->config('swoosh.settings')->get('endpoint'),
    ];
    $form['roles'] = [
      "#type" => "checkboxes",
      "#title" => $this->t(
              "Enable the beacon on all pages where the user has the following roles"
      ),
      "#default_value" => $this->config('swoosh.settings')->get("roles") ? $this->config('swoosh.settings')->get("roles") : [],
      "#options" => array_map("\Drupal\Component\Utility\Html::escape", user_role_names()),
      "#description" => $this->t(
              "If you select no roles, the condition will evaluate to TRUE for all users."
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('swoosh.settings')
      ->set('site', $form_state->getValue('site'))
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->set('roles', $form_state->getValue('roles'))
      ->set('status', $form_state->getValue('status'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
